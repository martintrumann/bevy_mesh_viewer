use bevy::{prelude::*, sprite::Mesh2dHandle};
use std::path::PathBuf;

#[derive(Debug, Resource)]
struct Reload(bool);
impl Reload {
    fn new() -> Self {
        Self(false)
    }
    fn flip(&mut self) {
        self.0 = !self.0
    }
}

#[derive(Debug, Resource)]
struct Path(PathBuf);

#[derive(Debug, Resource)]
struct PointVec(Vec<[f32; 3]>, Vec<u32>);

pub fn main() {
    let path = std::env::args().nth(1).unwrap().into();

    App::new()
        .add_plugins(DefaultPlugins.set(WindowPlugin {
            primary_window: Some(Window {
                title: "Mesh viewer --DEV--".into(),
                ..Default::default()
            }),
            ..Default::default()
        }))
        .insert_resource(Reload::new())
        .insert_resource(Path(path))
        .insert_resource(PointVec(Vec::new(), Vec::new()))
        .add_systems(Startup, cam)
        .add_systems(
            Update,
            (
                reload,
                export,
                hide_points,
                load_mesh.run_if(resource_changed::<Reload>()),
            ),
        )
        .run();
}

fn hide_points(inp: Res<Input<KeyCode>>, mut q: Query<&mut Visibility, With<Points>>) {
    if !inp.just_pressed(KeyCode::P) {
        return;
    }
    let mut v = q.single_mut();
    if *v != Visibility::Hidden {
        *v = Visibility::Hidden
    } else {
        *v = Visibility::Inherited
    }
}

fn export(inp: Res<Input<KeyCode>>, r: Res<PointVec>) {
    if !inp.just_pressed(KeyCode::E) {
        return;
    }

    let (v, i) = (&r.0, &r.1);

    println!(
        "
let mut m = Mesh::new(bevy::render::render_resource::PrimitiveTopology::TriangleList);
m.insert_attribute(Mesh::ATTRIBUTE_POSITION, vec!{v:?});
m.set_indices(Some(bevy::render::mesh::Indices::U32(vec!{i:?})));
m
"
    );
}

#[derive(Component)]
struct Points;

fn load_mesh(
    mut c: Commands,
    path: Res<Path>,
    mut q: Query<&mut Mesh2dHandle, With<MeshPreview>>,
    mut meshes: ResMut<Assets<Mesh>>,
    text: Query<Entity, With<Points>>,
    mut vec: ResMut<PointVec>,
) {
    let mesh = std::fs::read_to_string(&path.0).unwrap();

    let mut v = Vec::new();
    let mut i = Vec::new();

    let t = text.single();
    c.entity(t).despawn_descendants();

    let mut i_i = 0;
    for l in mesh.lines() {
        if l.is_empty() || l.starts_with('#') {
            continue;
        }

        let mut iter = l.split(',');
        let s1 = iter.next().unwrap().trim();
        let s2 = iter.next().unwrap().trim();
        if let Some(s3) = iter.next().map(str::trim) {
            i.push(s1.parse().unwrap());
            i.push(s2.parse().unwrap());
            i.push(s3.parse().unwrap());
        } else {
            let pos = [s1.parse().unwrap(), s2.parse().unwrap(), 0.];
            v.push(pos);
            c.spawn(Text2dBundle {
                text: Text::from_section(
                    format!("{i_i}"),
                    TextStyle {
                        font_size: 20.,
                        color: Color::WHITE,
                        ..Default::default()
                    },
                ),
                text_anchor: bevy::sprite::Anchor::BottomCenter,
                transform: Transform::from_translation(Vec3::from(pos) * SCALE),
                ..Default::default()
            })
            .set_parent(t);
            i_i += 1;
        }
    }

    *vec = PointVec(v.clone(), i.clone());

    let mut mesh = Mesh::new(bevy::render::render_resource::PrimitiveTopology::TriangleList);
    mesh.insert_attribute(Mesh::ATTRIBUTE_POSITION, v);
    mesh.set_indices(Some(bevy::render::mesh::Indices::U32(i)));

    let h = meshes.add(mesh);
    for mut n in q.iter_mut() {
        n.0 = h.clone();
    }
}

fn reload(mut r: ResMut<Reload>, inp: Res<Input<KeyCode>>) {
    if inp.just_pressed(KeyCode::R) {
        r.flip();
    }
}

#[derive(Component)]
struct MeshPreview;

const SCALE: Vec3 = Vec3::new(4., 4., 1.);

fn cam(mut c: Commands, mut meshes: ResMut<Assets<Mesh>>, mut mat: ResMut<Assets<ColorMaterial>>) {
    c.spawn(Camera2dBundle::default());

    c.spawn(ColorMesh2dBundle {
        transform: Transform::from_xyz(0., 0., 1.).with_scale(SCALE),
        ..Default::default()
    })
    .insert(MeshPreview);

    c.spawn(ColorMesh2dBundle {
        transform: Transform::from_xyz(530., 0., 1.).with_scale(Vec3::new(0.6, 0.6, 1.)),
        ..Default::default()
    })
    .insert(MeshPreview);

    let mut mesh = Mesh::new(bevy::render::render_resource::PrimitiveTopology::TriangleList);
    mesh.insert_attribute(
        Mesh::ATTRIBUTE_POSITION,
        vec![
            [-110., 110., 0.],
            [-100., 100., 0.],
            [-100., -100., 0.],
            [-110., -110., 0.],
            [100., -100., 0.],
            [110., -110., 0.],
            [100., 100., 0.],
            [110., 110., 0.],
        ],
    );
    mesh.set_indices(Some(bevy::render::mesh::Indices::U32(vec![
        0, 1, 2, //
        0, 2, 3, //
        2, 3, 4, //
        3, 4, 5, //
        4, 5, 6, //
        5, 6, 7, //
        0, 1, 6, //
        0, 6, 7, //
    ])));

    c.spawn(ColorMesh2dBundle {
        material: mat.add(Color::hsl(0., 0., 0.45).into()),
        mesh: meshes.add(mesh).into(),
        transform: Transform::from_scale(SCALE),
        ..Default::default()
    });

    c.spawn(SpatialBundle::from_transform(Transform::from_xyz(
        0., 0., 1.,
    )))
    .insert(Points);
}
